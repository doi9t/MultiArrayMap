/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.collections;

import ca.watier.utils.models.KeyValueHolder;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

public interface MultiValueCollection<K, V> extends Serializable, Iterable<KeyValueHolder<K, V>> {

    /**
     * Method to check the size of the collection.
     */
    int size();

    boolean containsKey(K key);

    /**
     * Method to fetch the values from a key.
     *
     * @param key - The key to be used
     * @return An unmodifiable List containing the values associated to the key, or null if the value is not present
     */
    Collection<Collection<V>> get(K key);

    /**
     * Method that adds a value to the specific key.
     *
     * @param key   The key
     * @param value The value
     */
    void add(K key, Collection<V> value);

    /**
     * Method that adds a value to the specific key.
     *
     * @param key   The key
     * @param value The value
     */
    void add(K key, V... value);

    /**
     * Method that adds a key with not values (Empty collection).
     *
     * @param key The key
     */
    void add(K key);

    /**
     * Remove the key and the values.
     *
     * @param key - The key to be removed
     * @return The List of values associated with the key to be removed
     */
    Collection<Collection<V>> remove(K key);

    void clear();

    /**
     * Method to removes the values for a specific key.
     *
     * @param key The key
     */
    void clear(K key);

    Set<K> keySet();

    /**
     * @return Method to return the values of all collections in a single list
     */
    Collection<V> values();

    boolean isEmpty();
}
