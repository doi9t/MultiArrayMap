/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.maps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * This implementation is the equivalent of having a {@link java.util.HashMap} with a {@link java.util.ArrayList} as values.
 * <br/><b>This collection is not synchronized and is not thread-safe with the current implementation.<b/>
 */
public class ArrayListMultiValueMapImpl<K, V> extends AbstractMultiValueMap<K, V> {

    private static final long serialVersionUID = 7762979475553893306L;
    private final int collectionSize;

    public ArrayListMultiValueMapImpl() {
        this(16, 16);
    }

    public ArrayListMultiValueMapImpl(int defaultMapSize, int defaultCollectionSize) {
        super(defaultMapSize);
        this.collectionSize = defaultCollectionSize;
    }

    @Override
    public List<V> get(K key) {
        Collection<V> collection = containerMap.get(key);

        if (collection != null) {
            return Collections.unmodifiableList((List<V>) collection);
        }

        return null;
    }

    @Override
    protected Collection<V> getAssociation(K key) {
        return containerMap.computeIfAbsent(key, k -> new ArrayList<>(collectionSize));
    }

    @Override
    public void put(K key) {
        containerMap.putIfAbsent(key, new ArrayList<>(collectionSize));
    }

    @Override
    public List<V> remove(K key) {
        return (List<V>) super.remove(key);
    }

    @Override
    public List<V> removeFrom(K key, V value) {
        return (List<V>) super.removeFrom(key, value);
    }

    /**
     * Method that returns all the values in a single list, allowing the duplicate, if present in multiple lists.
     */
    @Override
    public List<V> values() {
        List<V> values = new ArrayList<>();

        for (Collection<V> value : containerMap.values()) {
            values.addAll(value);
        }

        return Collections.unmodifiableList(values);
    }

    @Override
    public String toString() {
        return "ArrayListMultiValueMapImpl{" +
                "containerMap=" + containerMap +
                '}';
    }
}
