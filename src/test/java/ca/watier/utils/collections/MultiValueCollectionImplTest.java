/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.collections;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class MultiValueCollectionImplTest {
    @Test
    void arrayList_size() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);

        // then
        assertThat(givenCollection.size()).isEqualTo(6);
    }

    @Test
    void linkedList_size() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);

        // then
        assertThat(givenCollection.size()).isEqualTo(6);
    }


    @Test
    void arrayDeque_size() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);

        // then
        assertThat(givenCollection.size()).isEqualTo(6);
    }


    @Test
    void linkedBlockingQueue_size() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);

        // then
        assertThat(givenCollection.size()).isEqualTo(6);
    }

    @Test
    void hashSet_size() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);


        // then
        assertThat(givenCollection.size()).isEqualTo(5);
    }


    @Test
    void arrayList_get() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();

        // when
        givenCollection.add(10);
        givenCollection.add(20);

        // then
        Collection<Collection<String>> toAssert = givenCollection.get(10);
        assertThat(toAssert).hasSize(1);
    }

    @Test
    void linkedList_get() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();

        // when
        givenCollection.add(10);
        givenCollection.add(20);

        // then
        Collection<Collection<String>> toAssert = givenCollection.get(10);
        assertThat(toAssert).hasSize(1);
    }

    @Test
    void arrayDeque_get() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();

        // when
        givenCollection.add(10);
        givenCollection.add(20);

        // then
        Collection<Collection<String>> toAssert = givenCollection.get(10);
        assertThat(toAssert).hasSize(1);
    }

    @Test
    void linkedBlockingQueue_get() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();

        // when
        givenCollection.add(10);
        givenCollection.add(20);

        // then
        Collection<Collection<String>> toAssert = givenCollection.get(10);
        assertThat(toAssert).hasSize(1);
    }

    @Test
    void hashSet_get() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();

        // when
        givenCollection.add(10);
        givenCollection.add(20);

        // then
        Collection<Collection<String>> toAssert = givenCollection.get(10);
        assertThat(toAssert).hasSize(1);
    }

    @Test
    void arrayList_add() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();

        // when
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
    }

    @Test
    void linkedList_add() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();

        // when
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
    }

    @Test
    void arrayDeque_add() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();

        // when
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
    }

    @Test
    void linkedBlockingQueue_add() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();

        // when
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
    }

    @Test
    void hashSet_add() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();

        // when
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
    }


    @Test
    void arrayList_remove() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.remove(30);

        // then
        assertThat(givenCollection.size()).isEqualTo(2);
    }

    @Test
    void linkedList_remove() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.remove(30);

        // then
        assertThat(givenCollection.size()).isEqualTo(2);
    }

    @Test
    void arrayDeque_remove() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.remove(30);

        // then
        assertThat(givenCollection.size()).isEqualTo(2);
    }

    @Test
    void linkedBlockingQueue_remove() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.remove(30);

        // then
        assertThat(givenCollection.size()).isEqualTo(2);
    }

    @Test
    void hashSet_remove() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));


        // when
        givenCollection.remove(30);

        // then
        assertThat(givenCollection.size()).isEqualTo(2);
    }


    @Test
    void arrayList_clear() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.clear();

        // then
        assertThat(givenCollection.size()).isEqualTo(0);
    }

    @Test
    void linkedList_clear() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.clear();

        // then
        assertThat(givenCollection.size()).isEqualTo(0);
    }

    @Test
    void arrayDeque_clear() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.clear();

        // then
        assertThat(givenCollection.size()).isEqualTo(0);
    }

    @Test
    void linkedBlockingQueue_clear() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.clear();

        // then
        assertThat(givenCollection.size()).isEqualTo(0);
    }

    @Test
    void hashSet_clear() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));


        // when
        givenCollection.clear();

        // then
        assertThat(givenCollection.size()).isEqualTo(0);
    }

    @Test
    void arrayList_clear_inner() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.clear(20);

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
        assertThat(givenCollection.get(1)).isEmpty();
    }

    @Test
    void linkedList_clear_inner() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.clear(20);

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
        assertThat(givenCollection.get(1)).isEmpty();
    }

    @Test
    void arrayDeque_clear_inner() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.clear(20);

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
        assertThat(givenCollection.get(1)).isEmpty();
    }

    @Test
    void linkedBlockingQueue_clear_inner() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));

        // when
        givenCollection.clear(20);

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
        assertThat(givenCollection.get(1)).isEmpty();
    }

    @Test
    void hashSet_clear_inner() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();
        givenCollection.add(10);
        givenCollection.add(20, "a", "b");
        givenCollection.add(30, Arrays.asList("A", "B"));


        // when
        givenCollection.clear(20);

        // then
        assertThat(givenCollection.size()).isEqualTo(3);
        assertThat(givenCollection.get(1)).isEmpty();
    }

    @Test
    void arrayList_keyset() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);

        // when
        Set<Integer> toAssert = givenCollection.keySet();

        // then
        assertThat(toAssert).containsOnly(10, 20, 13, 14, 33);
    }

    @Test
    void linkedList_keyset() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);

        // when
        Set<Integer> toAssert = givenCollection.keySet();

        // then
        assertThat(toAssert).containsOnly(10, 20, 13, 14, 33);
    }


    @Test
    void arrayDeque_keyset() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);

        // when
        Set<Integer> toAssert = givenCollection.keySet();

        // then
        assertThat(toAssert).containsOnly(10, 20, 13, 14, 33);
    }


    @Test
    void linkedBlockingQueue_keyset() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);

        // when
        Set<Integer> toAssert = givenCollection.keySet();

        // then
        assertThat(toAssert).containsOnly(10, 20, 13, 14, 33);
    }

    @Test
    void hashSet_keyset() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();
        givenCollection.add(10);
        givenCollection.add(20);
        givenCollection.add(13);
        givenCollection.add(14);
        givenCollection.add(33);
        givenCollection.add(10);

        // when
        Set<Integer> toAssert = givenCollection.keySet();

        // then
        assertThat(toAssert).containsOnly(10, 20, 13, 14, 33);
    }

    @Test
    void arrayList_values() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        Collection<String> toAssert = givenCollection.values();

        // then
        assertThat(toAssert).containsExactly("A", "B", "C", "D", "E", "F", "G", "A", "B");
    }

    @Test
    void linkedList_values() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        Collection<String> toAssert = givenCollection.values();

        // then
        assertThat(toAssert).containsExactly("A", "B", "C", "D", "E", "F", "G", "A", "B");
    }


    @Test
    void arrayDeque_values() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        Collection<String> toAssert = givenCollection.values();

        // then
        assertThat(toAssert).containsExactly("A", "B", "C", "D", "E", "F", "G", "A", "B");
    }


    @Test
    void linkedBlockingQueue_values() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        Collection<String> toAssert = givenCollection.values();

        // then
        assertThat(toAssert).containsExactly("A", "B", "C", "D", "E", "F", "G", "A", "B");
    }

    @Test
    void hashSet_values() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        Collection<String> toAssert = givenCollection.values();

        // then
        assertThat(toAssert).containsExactlyInAnyOrder("A", "B", "C", "D", "E", "F", "G", "A", "B");
    }

    @Test
    void arrayList_containsKey_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        boolean toAssert = givenCollection.containsKey(10);

        // then
        assertThat(toAssert).isTrue();
    }

    @Test
    void linkedList_containsKey_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        boolean toAssert = givenCollection.containsKey(10);

        // then
        assertThat(toAssert).isTrue();
    }


    @Test
    void arrayDeque_containsKey_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        boolean toAssert = givenCollection.containsKey(10);

        // then
        assertThat(toAssert).isTrue();
    }


    @Test
    void linkedBlockingQueue_containsKey_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        boolean toAssert = givenCollection.containsKey(10);

        // then
        assertThat(toAssert).isTrue();
    }

    @Test
    void hashSet_containsKey_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        boolean toAssert = givenCollection.containsKey(10);

        // then
        assertThat(toAssert).isTrue();
    }


    @Test
    void arrayList_containsKey_not_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayList();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        boolean toAssert = givenCollection.containsKey(30);

        // then
        assertThat(toAssert).isFalse();
    }

    @Test
    void linkedList_containsKey_not_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedList();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");

        // when
        boolean toAssert = givenCollection.containsKey(30);

        // then
        assertThat(toAssert).isFalse();
    }


    @Test
    void arrayDeque_containsKey_not_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.arrayDeque();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        boolean toAssert = givenCollection.containsKey(30);

        // then
        assertThat(toAssert).isFalse();
    }


    @Test
    void linkedBlockingQueue_containsKey_not_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.linkedBlockingQueue();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        boolean toAssert = givenCollection.containsKey(30);

        // then
        assertThat(toAssert).isFalse();
    }

    @Test
    void hashSet_containsKey_not_found() {
        // given
        MultiValueCollection<Integer, String> givenCollection = MultiValueCollectionImpl.hashSet();
        givenCollection.add(10, "A", "B", "C");
        givenCollection.add(20, "D", "E", "F");
        givenCollection.add(20, "G", "A", "B");


        // when
        boolean toAssert = givenCollection.containsKey(30);

        // then
        assertThat(toAssert).isFalse();
    }


}