/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.maps;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Yannick on 5/4/2016.
 */
public class ArrayListMultiValueMapImplTest {

    private MultiValueMap<Integer, String> map;

    @BeforeEach
    void setUp() {
        map = new ArrayListMultiValueMapImpl<>();
    }

    @Test
    void putAll_null_key() {
        // given
        List<String> givenList = List.of("596");

        // when
        map.putAll(null, givenList);

        // then
        assertThat(map.get(null)).isEqualTo(givenList);
    }

    @Test
    void putEmpty() {
        // when
        map.put(10);

        // then
        assertThat(map.get(10)).isEmpty();
    }


    @Test
    void putEmpty_null_key() {
        // when
        map.put(null);

        // then
        assertThat(map.get(null)).isEmpty();
    }

    @Test
    void putEmpty_and_add_value_after() {
        // when
        map.put(10);
        map.put(10, "dsasdsd");

        // then
        assertThat(map.get(10)).isNotEmpty();
    }


    @Test
    void clearValue() {
        // given
        int givenFirstKey = 10;
        int givenSecondKey = 11;

        map.put(givenFirstKey, "sdhgsda");
        map.put(givenFirstKey, "e3445");
        map.put(givenFirstKey, "2522");
        map.put(givenSecondKey, "abcjdj");
        map.put(givenSecondKey, "abcjdj");

        // when
        map.clear(givenFirstKey);

        // then
        assertThat(map.get(givenFirstKey)).isEmpty();
        assertThat(map.get(givenSecondKey)).isNotEmpty();
    }

    @Test
    public void removeFromList() {
        // when
        map.put(10, "10");
        map.put(20, "20");
        map.put(20, "21");
        map.put(20, "22");
        map.put(30, "30");
        map.put(40, "40");

        // then
        assertThat(map.get(20)).containsOnly("20", "21", "22");
        map.removeFrom(20, "21");
        assertThat(map.get(20)).containsOnly("20", "22");
    }

    @Test
    public void addItemTest() {
        // when
        map.put(10, "10");
        map.put(20, "20");
        map.put(30, "30");
        map.put(40, "40");

        // then
        assertThat(map.size() == 4).isTrue();
    }

    @Test
    public void addItemWithSameKeyTest() {
        // when
        map.put(10, "10");
        map.put(10, "10.1");
        map.put(10, "10.2");
        map.put(10, "10.3");
        map.put(10, "10.4");
        map.put(20, "20");
        map.put(30, "30");
        map.put(40, "40");
        map.put(40, "40.1");
        map.put(40, "40.2");

        // then
        List<String> valuesTen = (List<String>) map.get(10);
        List<String> valuesForty = (List<String>) map.get(40);

        assertThat(map.size() == 4).isTrue();
        assertThat(valuesTen.size() == 5).isTrue();
        assertThat(valuesForty.size() == 3).isTrue();
    }

    @Test
    public void mergeTest() {
        // given
        map.put(10, "10");
        map.put(20, "20");
        map.put(30, "30");
        map.put(40, "40");

        MultiValueMap<Integer, String> map2 = new ArrayListMultiValueMapImpl<>();
        map2.put(50, "50");
        map2.put(60, "60");
        map2.put(70, "70");
        map2.put(80, "80");

        // when
        map.putAll(map2);

        // then
        assertThat(map.size() == 8).isTrue();
    }

    @Test
    public void removeItemTest() {
        // given
        map.put(10, "10");
        map.put(10, "10.1");
        map.put(10, "10.2");
        map.put(10, "10.3");
        map.put(10, "10.4");
        map.put(20, "20");
        map.put(30, "30");
        map.put(40, "40");
        map.put(40, "40.1");
        map.put(40, "40.2");

        // when
        map.remove(10);
        map.remove(40);

        // then
        assertThat(map.size() == 2).isTrue();
    }

    @Test
    public void valueNotPresentTest() {
        // when
        map.put(10, "10");

        // then
        assertThat(map.get(20) == null).isTrue();
    }

    @Test
    public void containsValueTest() {
        // when
        map.put(10, "10");

        // then
        assertThat(map.containsValue("10")).containsOnly(10);
    }

    @Test
    public void containsKeyTest() {
        // when
        map.put(10, "10");

        // then
        assertThat(map.containsKey(10)).isTrue();
    }

    @Test
    public void clearTest() {
        // given
        map.put(10, "10");

        // when
        map.clear();

        // then
        assertThat(map.isEmpty()).isTrue();
    }

    @Test
    public void keySetTest() {
        // when
        map.put(10, "10");
        map.put(20, "10");

        // then
        assertThat(map.keySet()).containsOnly(10, 20);
    }

    @Test
    public void valuesTest() {
        // when
        map.put(10, "10");
        map.put(10, "10.1");
        map.put(10, "10.2");
        map.put(10, "10.3");
        map.put(10, "10.4");
        map.put(20, "20");
        map.put(30, "30");
        map.put(40, "40");
        map.put(40, "40.1");
        map.put(40, "40.2");

        // then
        assertThat(map.values()).containsOnly("10", "10.1", "10.2", "10.3", "10.4", "20", "30", "40", "40.1", "40.2");
    }
}